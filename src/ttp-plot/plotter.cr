module Plotter 
  include SVG_LIB
  extend self
  @@city_radious = 10.0
  @@path_width = 1.0
  def city_radious=(value)
    @@city_radious = value
  end
  def city_radious
    @@city_radious
  end 

  def path_width=(value)
    @@path_width = value 
  end  

  def points_to_pixels(pts,width,height,pad = [0])

    xpad = pad[0].to_f / width
    ypad = (pad.size > 1)? (pad[1].to_f / height) : xpad

    xmn, xmx = pts.minmax_by{|p| p[0]}.map{|(x,y)| x} 
    ymn, ymx = pts.minmax_by{|p| p[1]}.map{|(x,y)| y}

    dx = xmx - xmn
    dy = ymx - ymn
    xmxval = (1.0 - 2.0*xpad) 
    ymxval = (1.0 - 2.0*ypad) 

    pixels = pts.map do |(x,y)| 
      px =  width*(xpad + (((x - xmn) / dx) * xmxval))
      py =  height*(ypad + (((ymx - y) / dy) * ymxval))
       
      {px,py} 
    end 
  end

  def cities(city_coord,radious = @@city_radious, 
    city_style = {fill: Color::BLACK},
    id_style = {fill: Color::WHITE, font_size: radious / 2.0, 
                text_anchor: Text_anchor::MIDDLE, 
                alignment_baseline: Alignment_baseline::CENTRAL, 
                font_weight: Font_weight::BOLDER }) 

    SVG.build do |g| 
      (1 ... city_coord.size).each do |i| 
        x,y = city_coord[i]
        g.push Circle.new(**city_style, cx: x, cy: y, r: radious)
        g.push Text.new(**id_style,text:(i+1).to_s, x: x, y: y)
      end 
      x,y = city_coord[0]
      g.push Circle.new(cx: x , cy: y, r: radious+1.0, 
                        fill: Color::LIGHTGREEN)
      g.push Text.new(**id_style,text:(1).to_s, x: x, y: y)
    end  
  end 

  def tour(tour,city_coord,city_wgt,mn_sw = @@path_width - 1.0, 
      mx_sw = @@path_width + 4, 
    tour_style = {fill: Color::BLACK,stroke: Color::BLACK})
    max_wgt  = city_wgt.reduce(0.0){|ac,w| ac + w }  
    SVG.build do |g| 
      acc_wgt = 0.0
      (0 ... tour.size).each do |i| 
        x1,y1 = city_coord[tour[i]]
        x2,y2 = city_coord[tour[(i+1) % tour.size]]
        acc_wgt += city_wgt[tour[i]]   

        wgt = mn_sw + (mx_sw * (acc_wgt.to_f / max_wgt))
        g.push Line.new(**tour_style,
          x1: x1, y1: y1, x2: x2, y2: y2, stroke_width: wgt)  
      end 
    end  
  end 

  def ratio(city_wgt,city_val,city_coord,city_in_plan,
    mn_r = @@city_radious-2,
    mx_r = @@city_radious+6,
    wgt_style = {fill: Paint::NONE, stroke: Color::GRAY, stroke_width: 2.0}, 
    val_style = {fill: Paint::NONE, stroke: Color::TURQUOISE, stroke_width: 2.0})
 
    importance = (0 ... city_wgt.size)
      .map{|i| city_wgt[i] + city_val[i]}
      .to_a
    max_val = importance.max

    SVG.build do |g| 
      (1 ... city_wgt.size).each do |i| 
        x,y = city_coord[i]
        radious = mn_r + (mx_r * importance[i]/ max_val) 

        perimeter  = 2.0 * Math::PI * radious
        ratio = city_val[i] / city_wgt[i]
      
        opacity = (city_in_plan[i]) ? 1.0 : 0.3 
        g.push Circle.new(**wgt_style,
          cx: x, cy: y, r: radious,
          opacity: opacity,
          transform: Transform.new.
            rotate(-90.0 + (360*(city_val[i]/importance[i])),x,y),
          stroke_dasharray: Stroke_dasharray.new(
            perimeter*(city_wgt[i] / importance[i]) , perimeter))
    
        g.push Circle.new(**val_style, 
          cx: x, cy: y, r: radious, 
          opacity: opacity,
          transform: Transform.new.rotate(-90.0,x,y),
          stroke_dasharray: Stroke_dasharray.new(
            (perimeter * (city_val[i] / importance[i])),perimeter))
      end 
    end 
  end 


  def legend
    w        = 250.0
    label_h  = 25.0
    pad      = 5.0
    h        = label_h * 3 + 2*pad 
    text_pos = 120.0
    city_r = 4.0
    return SVG.build(viewBox: ViewBox.new(0.0,0.0,w,h)) do |svg| 
      y = 0.0
    
      # Texto inicio-fin  
      cy = label_h / 2.0 
      svg.push(SVG.build(x:0.0,y: y, width: w, height: label_h) do |origin|
        
        origin.push Circle.new(cx: 50.0, cy: cy, r: 2*city_r,
          stroke:         Color::BLACK, 
          fill:       Color::SEAGREEN,
          stroke_width: 1.0)

        origin.push Text.new(text: "Inicio - Fin", x: text_pos, y: cy, 
          font_size:          label_h,
          alignment_baseline: Alignment_baseline::CENTRAL)
    
      end)

      y = label_h + pad 
  
      # Tamaño del circulo 
      svg.push(SVG.build(x:0.0,y: y, width: w, height: label_h) do |score|
        sw = 3.0
        city_r = 4.0

        perimeter = 2.0*Math::PI*city_r
        score.push Circle.new(cx: 15.0, cy: cy , r: city_r,
          fill: Paint::NONE,
          stroke: Color::GRAY,
          transform: Transform.new.rotate(-90.0 + 360 * 0.5,15.0,cy),
          stroke_dasharray: Stroke_dasharray.new((perimeter*(0.5)),perimeter),
          stroke_width: sw)
        score.push Circle.new(cx: 15.0, cy: cy , r: city_r,
          fill: Paint::NONE,
          stroke: Color::CYAN,
          transform: Transform.new.rotate(-90.0,15.0,cy),
          stroke_dasharray: Stroke_dasharray.new((perimeter*(0.5)),perimeter),
          stroke_width: sw)
        score.push Circle.new(cx: 15.0, cy: cy, r: city_r / 2.0,
                              fill: Color::BLACK) 
        
        score.push Text.new(text: "&lt;", x: 50.0, y: cy,
          font_size: label_h,
          text_anchor: Text_anchor::MIDDLE,
          alignment_baseline: Alignment_baseline::CENTRAL) 

        perimeter = 2.0*Math::PI*city_r*2.0
        score.push Circle.new(cx: 85.0, cy: cy , r: city_r*2.0,
          fill: Paint::NONE,
          stroke: Color::GRAY,
          transform: Transform.new.rotate(-90.0 + 360 * 0.5,85.0,cy),
          stroke_dasharray: Stroke_dasharray.new((perimeter*(0.5)),perimeter),
          stroke_width: sw)
        score.push Circle.new(cx: 85.0, cy: cy , r: city_r*2.0,
          fill: Paint::NONE,
          stroke: Color::CYAN,
          transform: Transform.new.rotate(-90.0,85.0,cy),
          stroke_dasharray: Stroke_dasharray.new((perimeter*(0.5)),perimeter),
          stroke_width: sw)
        score.push Circle.new(cx: 85.0, cy: cy, r: city_r / 2.0,
                              fill: Color::BLACK) 
        
        score.push Text.new(text: "Peso-Valor",x: text_pos , y: cy, 
          font_size: label_h, 
          alignment_baseline: Alignment_baseline::CENTRAL) 
      end)

      y = label_h * 2 + pad 
      svg.push(
        SVG.build(x: 0.0, y: y, width: w, height: label_h) do |path|

          path.push Line.new(x1: 0.0, y1: cy, x2: 30.0, y2: cy, 
            stroke: Color::BLACK,
            stroke_width: 2.0)
          
          path.push Text.new(text: "&lt;", x: 50.0, y: cy,
            font_size: label_h , 
            alignment_baseline: Alignment_baseline::CENTRAL, 
            text_anchor: Text_anchor::MIDDLE)

          path.push Line.new(x1: 70.0, y1: cy, x2: 100.0, y2: cy, 
            stroke: Color::BLACK,
            stroke_width: 10.0)
          
         path.push Text.new(text: "Peso", x: text_pos, y: cy,
            font_size: label_h, 
            alignment_baseline: Alignment_baseline::CENTRAL)
        end) 
    end 
  end 


  def title(text : String,**style)
    w = 500.0
    h = 25.0
    SVG.build(viewBox: ViewBox.new(0.0,0.0,w,h)) do |title|
     title.push Text.new(**style, 
        x: w/2.0,
        y: h / 2.0,
        font_size: h,
        alignment_baseline: Alignment_baseline::CENTRAL,
        text_anchor: Text_anchor::MIDDLE,
        text: text)
    end 
  end 

end 
