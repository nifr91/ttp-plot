#!/usr/bin/env crystal 

# Ricardo Nieto Fuentes

require "ttp-inst"
require "svg-lib"
require "./ttp-plot/*"

help = <<-HELP
  ./script -- solution
HELP

if ARGV.size == 0 || ARGV.any?{|arg| (arg == "-h") || (arg == "--help")} 
  puts help 
  exit 1
end 

file_name = ARGV[0] 

unless File.exists?(file_name) 
  puts "No existe el archivo ARGV[0]" 
  exit 1 
end 

include SVG_LIB

sol, inst = Thief.load(file_name) 
ttp = TTP.load("instances/#{inst}.ttp")



# Plotter.city_radious = 8.0 
Plotter.city_radious = {8.0 *  ((3_000 - ttp.ncities)/ 3_000.0),2.0}.max

puts %(<?xml version="1.0" encoding="UTF-8" standalone="no"?>)
puts %(<!-- Created with ttp-plot -->)

w = h = 1000.0
title_space = 100.0

STDOUT << SVG.build(viewBox: ViewBox.new(0.0,0.0,w,h)) do |plot| 

  # # Graficar un rectángulo para mostrar los límites
  # plot.push Rect.new(
  # x: 0.0, y: 0.0, width: w , height: h, fill: Paint::NONE,
  # stroke: Color::BLACK,
  # stroke_width: 0.5)

  # # Graficar el título 
  # t = Plotter.title("#{inst}").try do |title|
  #   title.width = 500.0
  #   title.height = 25.0
  #   title.x = w - 500.0
  #   title.y = title_space / 2.0 - (25.0/2.0)
  #   title 
  # end 
  # plot.push t

  # # Graficar el título 
  # l = Plotter.legend.try do |legend|
  #   legend.x = 0.0
  #   legend.width = 250.0
  #   legend.height = 70.0
  #   legend.y = (title_space - 70.0) / 2.0
  #   legend
  # end 
  # plot.push l 

  # Coordenadas en pixeles de las ciudades
  cities = ttp.cities.map{|c| {c.x.to_f64,c.y.to_f64} } 


  # Obtener los datos necesarios 
  city_coord = Plotter.points_to_pixels(
    cities,w,h,pad: [2.5*Plotter.city_radious])
  # Peso en cada ciudad
  city_wgt = ttp.cities_wgt(sol.plan)


  # Graficar el recorrido
  tour = Plotter.tour(sol.tour,city_coord,city_wgt).try do |tour|
    tour.x = 0.0
    tour.y = 0.0
    tour.width = w
    tour.height = h
    tour
  end 
  plot.push tour


  # # Obtener las ganancias por ciudad 
  # city_in_plan = Array.new(ttp.ncities){false} 
  # ttp.items.each{|item| city_in_plan[item.city] = true  if sol.plan[item.id]} 
  # city_val = Array.new(ttp.ncities){0.0} 
  # city_wgt = Array.new(ttp.ncities){0.0} 
  # ttp.items.each do |item| 
  #   if city_in_plan[item.city]
  #     if sol.plan[item.id] 
  #       city_val[item.city] += item.val
  #       city_wgt[item.city] += item.wgt 
  #     end 
  #   else 
  #     city_val[item.city] += item.val
  #     city_wgt[item.city] += item.wgt 
  #   end
  # end 

  # # Graficar la relación peso-ganancia 
  # ratio = Plotter.ratio(city_wgt, city_val, city_coord,city_in_plan).try do |ratio|
  #   ratio.x      = 0.0
  #   ratio.y      = 0.0
  #   ratio.width  = w
  #   ratio.height = h
  #   ratio
  # end 
  # plot.push ratio 



  # Graficar las ciudades 
  cities = Plotter.cities(city_coord).try do |cities| 
    cities.x = 0.0
    cities.y = 0.0
    cities.width = w
    cities.height = h
    cities
  end 
  plot.push cities
 
    


end 
